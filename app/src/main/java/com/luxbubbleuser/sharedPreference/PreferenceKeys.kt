package com.luxbubbleuser.sharedPreference

object PreferenceKeys {

    var preferenceName = "com.luxbubble"
    var foreverPreferenceName = "luxbubble_forever"

    var social = "social"
    var guest = "guest"
    var welcome = "welcome"

    var email = "email"
    var pass = "pass"
    var code = "code"

    var loginData = "loginData"
    var rememberMe = "rememberMe"
}