package com.luxbubbleuser.sharedPreference

import android.content.Context
import android.content.SharedPreferences

object PreferenceFile {

    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var foreverSharedPreferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor

    fun storeKey(context: Context, key: String, value: String) {
        sharedPreferences = context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
        editor.commit()
    }

    fun retrieveKey(context: Context, key: String): String? {
        sharedPreferences = context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        return sharedPreferences.getString(key, null)
    }

    fun storeBoolean(context: Context, key: String, value: Boolean) {
        sharedPreferences = context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun retrieveBooleanKey(context: Context, key: String): Boolean? {
        sharedPreferences = context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        return sharedPreferences.getBoolean(key, false)
    }

/*
    fun storeLoginData(context: Context, loginResponse: UserDetailModel) {
        sharedPreferences = context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        val prefsEditor: SharedPreferences.Editor = sharedPreferences.edit()
        prefsEditor.putString(PreferenceKeys.loginData, Gson().toJson(loginResponse))
        prefsEditor.apply()
        prefsEditor.commit()
    }

    fun retrieveLoginData(context: Context): UserDetailModel? {
        sharedPreferences = context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        return Gson().fromJson(sharedPreferences.getString(PreferenceKeys.loginData, "")!!, UserDetailModel::class.java)
    }
*/

    fun clearPreference(context: Context) {
        sharedPreferences = context.getSharedPreferences(PreferenceKeys.preferenceName, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }

    fun storeForeverKey(context: Context, key: String, value: String) {
        foreverSharedPreferences = context.getSharedPreferences(PreferenceKeys.foreverPreferenceName, Context.MODE_PRIVATE)
        editor = foreverSharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()

    }

    fun retrieveForeverKey(context: Context, key: String): String? {
        foreverSharedPreferences = context.getSharedPreferences(PreferenceKeys.foreverPreferenceName, Context.MODE_PRIVATE)
        return foreverSharedPreferences.getString(key, null)
    }

}