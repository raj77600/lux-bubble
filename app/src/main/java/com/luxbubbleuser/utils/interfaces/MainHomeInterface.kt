package com.luxbubbleuser.utils.interfaces

interface MainHomeInterface {

    fun setClassName(string: String)
    fun bottomBar(boolean: Boolean)
    fun backPress()

}