package com.luxbubbleuser.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import com.google.android.material.snackbar.Snackbar
import com.luxbubbleuser.R
import com.luxbubbleuser.view.login.loginNumber.LoginFirst


object CommonAlerts {

    lateinit var progressBar: CustomProgressDialog

    fun showProgress(message: String, context: Context) {
        Log.e("progress dialog", "progressdialog")
        progressBar = CustomProgressDialog(context)
        progressBar.show()
        progressBar.setCancelable(true)
        progressBar.setCanceledOnTouchOutside(true)
    }

    fun showLoginAppPopUp(context: Context) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(context.resources.getString(R.string.app_name))
        builder.setCancelable(true)
        builder.setMessage("Please login to continue")
        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            val intent = Intent(context, LoginFirst::class.java)
            (context as Activity).startActivity(intent)
        }
        builder.show()
    }

    fun hideProgress() {
        try {
            if (progressBar.isShowing) {
                progressBar.dismiss()
            }
        } catch (e: Exception) {
        }
    }

    fun showSnackbar(message: String, context: Context) {
        if (message.isNotEmpty()) {
            Snackbar.make(
                (context as Activity).findViewById(android.R.id.content),
                message,
                Snackbar.LENGTH_SHORT
            ).show()
        }
    }

    fun isValidEmail(target: CharSequence): Boolean {
        return (android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches())
    }

    fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}