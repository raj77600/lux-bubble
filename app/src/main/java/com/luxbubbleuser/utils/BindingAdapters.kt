package com.luxbubbleuser.utils

import android.content.Context
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableField
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayout
import com.hbb20.CountryCodePicker
import com.luxbubbleuser.R
import com.luxbubbleuser.networkcalls.Urls
import de.hdodenhof.circleimageview.CircleImageView

/** Binding Adapters */
object BindingAdapters {

    @BindingAdapter(value = ["viewPagerAdapter", "setupDotsIndicator"], requireAll = false)
    @JvmStatic
    fun setViewPagerAdapter(viewPager: ViewPager, adapter: PagerAdapter, dotsIndicator: TabLayout)
    {
        viewPager.clipToPadding = false
        viewPager.setPageTransformer(false) { page, position ->
            page.translationX = -(0.25f * position)
            page.scaleY = 1 - (0.25f * kotlin.math.abs(position))
        }
        viewPager.adapter = adapter
        dotsIndicator.setupWithViewPager(viewPager)
    }

    @BindingAdapter(value = ["setRecyclerAdapter"], requireAll = false)
    @JvmStatic
    fun setRecyclerAdapter(recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>) {
        recyclerView.adapter = adapter
    }

    @BindingAdapter(value = ["setSelectedCountryCode"], requireAll = false)
    @JvmStatic
    fun setSelectedCountryCode(countryCodePicker: CountryCodePicker, observableField: ObservableField<Int>) {
        countryCodePicker.setCountryForPhoneCode(observableField.get()!!)
    }

    @BindingAdapter(value = ["setImage"], requireAll = false)
    @JvmStatic
    fun setImage(circleImageView: CircleImageView, observableField: ObservableField<String>) {
        if (observableField.get() != "") {
            Glide.with(circleImageView.context!!).load(Urls.BASE_URL + observableField.get())
                    .into(circleImageView)
        }
    }

}