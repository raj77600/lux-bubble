package com.luxbubbleuser.utils

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Build
import android.view.View
import android.view.animation.AnimationUtils
import androidx.annotation.RequiresApi
import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.luxbubbleuser.R
import com.luxbubbleuser.alerter.Alerter
import com.mansour.com.validate.ValidatorUtils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

//val discover = Discover()

/* Fragment Transaction */
/*fun Context.loadFragment(fragment: Fragment, bundle: Bundle?) {

    ValidatorUtils.hideSoftKeyboard(this as Activity)

    if (bundle != null) {
        fragment.arguments = bundle
    }
    val fragmentManager =
        (this as FragmentActivity).supportFragmentManager.beginTransaction()

    fragmentManager
        .setCustomAnimations(
            R.anim.enter,
            R.anim.exit,
            R.anim.pop_enter,
            R.anim.pop_exit
        )
        .replace(R.id.flHome, fragment)
        .addToBackStack(null)
        .commit()

}*/

/*ActivityTransaction*/
fun Context.openActivity(intent: Intent, finishAffinity: Boolean) {
    ValidatorUtils.hideSoftKeyboard(this as Activity)
    startActivity(intent)
//    this.overridePendingTransition(R.anim.slide_up, R.anim.slide_up)
    if (finishAffinity) this.finishAffinity()
}

/* backPress */
fun Context.goBack() {
    ValidatorUtils.hideSoftKeyboard(this as Activity)
    this.onBackPressed()
}

/* monthName */
fun getMonthName(int: Int): String {
    return when (int) {
        1 -> "January"
        2 -> "February"
        3 -> "March"
        4 -> "April"
        5 -> "May"
        6 -> "June"
        7 -> "July"
        8 -> "August"
        9 -> "September"
        10 -> "October"
        11 -> "November"
        12 -> "December"
        else -> ""
    }
}

/* Status Bar Color Changer */
@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun Context.setStatusBarColor(color: Int) {
    try {
        (this as Activity).window.statusBarColor = getResources().getColor(color)
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

/* Internet Connection Detector */
fun Context.isNetworkAvailable(): Boolean {
    val service = Context.CONNECTIVITY_SERVICE
    val manager = getSystemService(service) as ConnectivityManager?
    val network = manager?.activeNetworkInfo
    return (network != null)
}

/* Positive Alerter*/
fun Context.showNegativeAlerter(message: String) {
    /* Snackbar.make(
         (this as Activity).findViewById(android.R.id.content),
         message,
         Snackbar.LENGTH_SHORT
     ).show()*/
    Alerter.create(this as Activity)
        .setTitle(message)
        .setBackgroundColorRes(R.color.alert_default_error_background)
        .show()
}

/* Negative Alerter*/
fun Context.showPositiveAlerter(message: String) {
    Alerter.create(this as Activity)
        .setTitle(message)
        .setBackgroundColorRes(R.color.alerter_default_success_background)
        .show()
}

/*ObservableDataClassToJson*/
fun <T> convertToJson(data: T): String {
    return Gson().toJson(data)
        .replace("{\"mValue\"", "").replace("},", ",")
}

/*Animation Util*/
fun Context.playAnim(view: View, anim: Int) {
    val myAnim = AnimationUtils.loadAnimation(this, anim)
    view.startAnimation(myAnim)
}

/*DatePicker*/
fun Context.selectDate(observableField: ObservableField<String>) {
    val calender: Calendar = Calendar.getInstance()
    val datePicker = DatePickerDialog(
        this,
        { view, year, month, dayOfMonth ->
            calender.set(Calendar.YEAR, year)
            calender.set(Calendar.MONTH, month)
            calender.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            observableField.set("$year-${month + 1}-$dayOfMonth")
        }, calender
            .get(Calendar.YEAR), calender.get(Calendar.MONTH),
        calender.get(Calendar.DAY_OF_MONTH)
    )
    datePicker.datePicker.maxDate = System.currentTimeMillis()
    datePicker.show()
}

/*string to part request body*/
fun getPartRequestBody(string: String): RequestBody {
    return string
        .toRequestBody("multipart/form-data".toMediaTypeOrNull())
}

/*File to Part*/
fun getPartFromFile(string: String, param: String): MultipartBody.Part {
    val file = File(string)
    val reqFile = file.asRequestBody("image/jpeg".toMediaTypeOrNull())
    return MultipartBody.Part.createFormData(param, file.name, reqFile)
}

/*Change Time Format*/
fun changeTimeFormat(input: String): String {

    var output = ""

    try {
        var simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")

        var date = simpleDateFormat.parse(input)
        var simpleDateFormat1 = SimpleDateFormat("dd-MM-yyyy")
        output = simpleDateFormat1.format(date!!)

    } catch (e: Exception) {
        e.printStackTrace()
    }

    return output

}


