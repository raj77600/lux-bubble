package com.luxbubbleuser.view.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.WindowManager
import com.luxbubbleuser.R
import com.luxbubbleuser.view.chooseLoginSignUp.ChooseLoginSignup

class Splash : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        Handler(Looper.getMainLooper()).postDelayed({

            startActivity(Intent(this, ChooseLoginSignup::class.java)
                .putExtra("type","signup"))
            finish()

        }, 2000)
    }
}