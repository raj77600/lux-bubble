package com.luxbubbleuser.view.setUpProfile

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.luxbubbleuser.view.congScreen.CongoScreen

class SetupProfileVM(val context: Context) : ViewModel() {

    fun click(value:String)
    {
        when(value)
        {
            "back" ->
            {
                (context as SetupProfile).onBackPressed()
            }
            "next" ->
            {
                context.startActivity(Intent(context, CongoScreen::class.java) )
            }
        }
    }
}