package com.luxbubbleuser.view.setUpProfile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.luxbubbleuser.R
import com.luxbubbleuser.databinding.ActivityOtpScreenBinding
import com.luxbubbleuser.databinding.ActivitySetupProfileBinding
import com.luxbubbleuser.view.otpScreen.OtpScreenFactory
import com.luxbubbleuser.view.otpScreen.OtpScreenVM

class SetupProfile : AppCompatActivity() {

    private var loginBinding: ActivitySetupProfileBinding? = null
    private lateinit var viewModel: SetupProfileVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_choose_login_signup)

        loginBinding = DataBindingUtil.setContentView(this, R.layout.activity_setup_profile)
        val factory = SetupProfileFactory(this)
        viewModel = ViewModelProvider(this, factory).get(SetupProfileVM::class.java)
        loginBinding?.viewModel = viewModel
    }
}