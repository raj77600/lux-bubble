package com.luxbubbleuser.view.setUpProfile

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class SetupProfileFactory(val context: Context) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SetupProfileVM::class.java)) {
            return SetupProfileVM(context) as T
        }
        throw IllegalArgumentException("")
    }
}