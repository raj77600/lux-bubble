package com.luxbubbleuser.view.login.loginNumber

import android.content.Context
import android.content.Intent
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.luxbubbleuser.view.login.loginPass.LoginPass
import com.luxbubbleuser.view.signUp.SignUp

class LoginFirstVM(val context: Context, val intent: Intent) : ViewModel() {

    var textHeading = ObservableField("Please enter Your phone number")
    var textHint = ObservableField("Phone Number")
    var codeBoolean = ObservableField(true)

    init
    {
        if (intent.hasExtra("loginType"))
        {
            if (intent.getStringExtra("loginType") == "email")
            {
                textHeading.set("Please enter Your Email Id")
                textHint.set("Email Id")
                codeBoolean.set(false)
            }
            else
            {
                textHeading.set("Please enter Your phone number")
                textHint.set("Phone Number")
                codeBoolean.set(true)
            }
        }
    }

    fun click(value:String)
    {
        when(value)
        {
            "back" ->
            {
                (context as LoginFirst).onBackPressed()
            }
            "next" ->
            {
                context.startActivity(Intent(context, LoginPass::class.java) )
            }
        }
    }
}