package com.luxbubbleuser.view.login.loginPass

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class LoginPassFactory(val context: Context) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginPassVM::class.java)) {
            return LoginPassVM(context) as T
        }
        throw IllegalArgumentException("")
    }
}