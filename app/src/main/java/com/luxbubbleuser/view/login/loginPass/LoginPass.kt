package com.luxbubbleuser.view.login.loginPass

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.luxbubbleuser.R
import com.luxbubbleuser.databinding.ActivityLoginFirstBinding
import com.luxbubbleuser.databinding.ActivityLoginPassBinding
import com.luxbubbleuser.view.login.loginNumber.LoginFirstFactory
import com.luxbubbleuser.view.login.loginNumber.LoginFirstVM

class LoginPass : AppCompatActivity() {

    private var loginBinding: ActivityLoginPassBinding? = null
    private lateinit var viewModel: LoginPassVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_choose_login_signup)

        loginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login_pass)
        val factory = LoginPassFactory(this)
        viewModel = ViewModelProvider(this, factory).get(LoginPassVM::class.java)
        loginBinding?.viewModel = viewModel
    }
}