package com.luxbubbleuser.view.login.loginNumber

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.luxbubbleuser.R
import com.luxbubbleuser.databinding.ActivityLoginFirstBinding

class LoginFirst : AppCompatActivity() {

    private var loginBinding: ActivityLoginFirstBinding? = null
    private lateinit var viewModel: LoginFirstVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_choose_login_signup)

        loginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login_first)
        val factory = LoginFirstFactory(this,intent)
        viewModel = ViewModelProvider(this, factory).get(LoginFirstVM::class.java)
        loginBinding?.viewModel = viewModel
    }
}