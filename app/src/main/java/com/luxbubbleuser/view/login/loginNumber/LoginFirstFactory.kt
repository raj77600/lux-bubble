package com.luxbubbleuser.view.login.loginNumber

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class LoginFirstFactory(val context: Context, val intent: Intent) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginFirstVM::class.java)) {
            return LoginFirstVM(context,intent) as T
        }
        throw IllegalArgumentException("")
    }
}