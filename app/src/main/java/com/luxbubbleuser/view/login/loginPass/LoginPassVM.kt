package com.luxbubbleuser.view.login.loginPass

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.widget.RadioButton
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModel
import com.luxbubbleuser.view.mainActivity.MainActivity
import com.luxbubbleuser.R
import com.luxbubbleuser.view.login.loginNumber.LoginFirst
import com.luxbubbleuser.view.otpScreen.OtpScreen

class LoginPassVM(val context: Context) : ViewModel() {

    var typeCheck = "email"

    fun showVerificationDialog()
    {
        val mDialogView = LayoutInflater.from(context).inflate(R.layout.choose_verifi_type_layout, null)

        val mBuilder = AlertDialog.Builder(context).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCanceledOnTouchOutside(true)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val tvSubmitChange: TextView = mDialogView.findViewById<View>(R.id.tvNext) as TextView
        val phoneNumber: RadioButton = mDialogView.findViewById<View>(R.id.phoneNumbner) as RadioButton
        val email2: RadioButton = mDialogView.findViewById<View>(R.id.email) as RadioButton

        phoneNumber.isChecked = true

        tvSubmitChange.setOnClickListener {

            if (phoneNumber.isChecked)
            {
                typeCheck = "phone"
            }
            else if (email2.isChecked)
            {
                typeCheck = "email"
            }

            context.startActivity(Intent(context, OtpScreen::class.java) )

            mAlertDialog.dismiss()
        }
    }

    fun click(value:String)
    {
        when(value)
        {
            "back" ->
            {
                (context as LoginFirst).onBackPressed()
            }
            "forgot" ->
            {
                showVerificationDialog()
            }
            "next" ->
            {
                context.startActivity(Intent(context, MainActivity::class.java) )
            }
        }
    }
}