package com.luxbubbleuser.view.chooseLoginSignUp

import android.content.Context
import android.content.Intent
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.luxbubbleuser.view.login.loginNumber.LoginFirst
import com.luxbubbleuser.view.signUp.SignUp

class ChooseLoginSignUpVM(val context: Context, val intent: Intent) : ViewModel() {

    var type = "signup"

    init
    {
        if (intent.hasExtra("type"))
        {
            if (intent.getStringExtra("type") == "signup")
            {
                type = "signup"
            }
            else
            {
                type = "login"
            }
        }
    }

    var text1 = ObservableField("Sign Up With Email Address")
    var text2 = ObservableField("Sign Up With Phone Number")
    var alreadyText = ObservableField("Already Have an Account ? Login")

    fun loginPart()
    {
        if (type == "login")
        {
            type = "signup"

            text1.set("Sign Up With Email Address")
            text2.set("Sign Up With Phone Number")
            alreadyText.set("Already Have an Account ? Login")
        }
        else
        {
            type = "login"

            text1.set("Login With Email")
            text2.set("Login With Phone Number")
            alreadyText.set("Doesn't Have An Account ? Signup")
        }
    }

    fun clicks(value:String)
    {
        when(value)
        {
            "email" ->
            {
                if (type == "login")
                {
                    context.startActivity(Intent(context, LoginFirst::class.java)
                        .putExtra("loginType","email"))
                }
                else
                {
                    context.startActivity(Intent(context, SignUp::class.java)
                        .putExtra("loginType","email"))
                }
            }
            "number" ->
            {
                if (type == "login")
                {
                    context.startActivity(Intent(context, LoginFirst::class.java)
                        .putExtra("loginType","number"))
                }
                else
                {
                    context.startActivity(Intent(context, SignUp::class.java)
                        .putExtra("loginType","email"))
                }
            }
        }
    }
}