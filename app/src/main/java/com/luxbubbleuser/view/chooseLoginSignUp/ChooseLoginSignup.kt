package com.luxbubbleuser.view.chooseLoginSignUp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.luxbubbleuser.R
import com.luxbubbleuser.databinding.ActivityChooseLoginSignupBinding

class ChooseLoginSignup : AppCompatActivity() {

    private var loginBinding: ActivityChooseLoginSignupBinding? = null
    private lateinit var viewModel: ChooseLoginSignUpVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_choose_login_signup)

        loginBinding = DataBindingUtil.setContentView(this, R.layout.activity_choose_login_signup)
        val factory = ChooseFactory(this,intent)
        viewModel = ViewModelProvider(this, factory).get(ChooseLoginSignUpVM::class.java)
        loginBinding?.viewModel = viewModel
    }
}