package com.luxbubbleuser.view.chooseLoginSignUp

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ChooseFactory(val context: Context, val intent: Intent) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ChooseLoginSignUpVM::class.java)) {
            return ChooseLoginSignUpVM(context,intent) as T
        }
        throw IllegalArgumentException("")
    }
}