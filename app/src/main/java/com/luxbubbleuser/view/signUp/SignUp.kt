package com.luxbubbleuser.view.signUp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.luxbubbleuser.R
import com.luxbubbleuser.databinding.ActivitySignUpBinding

class SignUp : AppCompatActivity() {

    private var loginBinding: ActivitySignUpBinding? = null
    private lateinit var viewModel: SignUpVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_choose_login_signup)

        loginBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up)
        val factory = SignUpFactory(this,intent)
        viewModel = ViewModelProvider(this, factory).get(SignUpVM::class.java)
        loginBinding?.viewModel = viewModel
    }
}