package com.luxbubbleuser.view.signUp

import android.content.Context
import android.content.Intent
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.luxbubbleuser.view.login.loginNumber.LoginFirst
import com.luxbubbleuser.view.login.loginPass.LoginPass
import com.luxbubbleuser.view.otpScreen.OtpScreen
import com.luxbubbleuser.view.setUpProfile.SetupProfile

class SignUpVM(val context: Context, val intent: Intent) : ViewModel() {

    var textHeading = ObservableField("")
    var textHint = ObservableField("Phone Number")
    var codeBoolean = ObservableField(false)

    init
    {
        if (intent.hasExtra("loginType"))
        {
            if (intent.getStringExtra("loginType") == "email")
            {
                textHeading.set("Please enter Your Email Id")
                textHint.set("Email Id")
                codeBoolean.set(false)
            }
            else
            {
                textHeading.set("Please enter Your phone number")
                textHint.set("Phone Number")
                codeBoolean.set(true)
            }
        }
    }

    fun click(value:String)
    {
        when(value)
        {
            "back" ->
            {
                (context as SignUp).onBackPressed()
            }
            "next" ->
            {
                context.startActivity(Intent(context, SetupProfile::class.java) )
            }
        }
    }
}