package com.luxbubbleuser.view.signUp

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class SignUpFactory(val context: Context, val intent: Intent) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SignUpVM::class.java)) {
            return SignUpVM(context,intent) as T
        }
        throw IllegalArgumentException("")
    }
}