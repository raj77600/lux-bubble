package com.luxbubbleuser.view.allowLocation

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class AllowLocFactory(val context: Context) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        if (modelClass.isAssignableFrom(AllowLocVM::class.java))
        {
            return AllowLocVM(context) as T
        }
        throw IllegalArgumentException("")
    }
}