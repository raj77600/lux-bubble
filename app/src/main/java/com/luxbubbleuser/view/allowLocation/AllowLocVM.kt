package com.luxbubbleuser.view.allowLocation

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.luxbubbleuser.view.enableNotification.EnableNotification

class AllowLocVM(val context: Context) : ViewModel() {

    fun clicks(value:String)
    {
        when(value)
        {
            "back" ->
            {
                (context as AllowLocation).onBackPressed()
            }
            "loc" -> {
                (context as AllowLocation).startActivity(Intent(context, EnableNotification::class.java))
//                (context).finish()
            }
        }
    }

}