package com.luxbubbleuser.view.allowLocation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.luxbubbleuser.R
import com.luxbubbleuser.databinding.ActivityAllowLocationBinding

class AllowLocation : AppCompatActivity() {
    private var forgotBinding: ActivityAllowLocationBinding? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        forgotBinding = DataBindingUtil.setContentView(this, R.layout.activity_allow_location)
        val factory = AllowLocFactory(this)
        val viewModel = ViewModelProvider(this, factory).get(AllowLocVM::class.java)
        forgotBinding?.viewModel = viewModel
    }
}