package com.luxbubbleuser.view.mainActivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.luxbubbleuser.R
import com.luxbubbleuser.sharedPreference.PreferenceFile
import com.luxbubbleuser.sharedPreference.PreferenceKeys
import com.luxbubbleuser.utils.CommonAlerts
import com.luxbubbleuser.utils.CommonMethods
import com.luxbubbleuser.utils.interfaces.MainHomeInterface
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() , MainHomeInterface {

    var selectedScreen = "home"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomBarClicks()
    }

    private fun bottomBarClicks()
    {
        bottomBar.setOnNavigationItemSelectedListener { item ->

            when (item.itemId)
            {
                R.id.page_1 -> {

                    if (selectedScreen != "home")
                    {
                        selectedScreen = "home"
                    }
                    true
                }
                R.id.page_2 -> {

                    if (selectedScreen != "trending")
                    {
                        selectedScreen = "trending"
                    }
                    true
                }
                R.id.page_3 -> {

                    if (selectedScreen != "booking")
                    {
                        selectedScreen = "booking"
                    }
                    true
                }
                R.id.page_4 -> {

                    if (selectedScreen != "chat")
                    {
                        selectedScreen = "chat"
                    }
                    true
                }
                R.id.page_5 -> {

                    if (selectedScreen != "account")
                    {
                        selectedScreen = "account"
                    }
                    true
                }
                else ->
                {
                    selectedScreen = ""
                    false
                }
            }
        }
    }

    override fun setClassName(string: String)
    {
        selectedScreen = string
        when (string)
        {
            "home" ->
            {
                bottomBar.selectedItemId = R.id.page_1
            }
            "trending" ->
            {
                bottomBar.selectedItemId = R.id.page_2
            }
            "booking" ->
            {
                bottomBar.selectedItemId = R.id.page_3
            }
            "chat" ->
            {
                bottomBar.selectedItemId = R.id.page_4
            }
            "account" ->
            {
                bottomBar.selectedItemId = R.id.page_5
            }
        }
    }

    override fun bottomBar(boolean: Boolean)
    {
        if (boolean)
        {
            bottomBar.visibility = View.VISIBLE
        }
        else
        {
            bottomBar.visibility = View.GONE
        }
    }

    override fun backPress() {
        onBackPressed()
    }
}