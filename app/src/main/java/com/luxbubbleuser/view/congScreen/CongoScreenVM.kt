package com.luxbubbleuser.view.congScreen

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.luxbubbleuser.view.mainActivity.MainActivity
import com.luxbubbleuser.view.setUpProfile.SetupProfile

class CongoScreenVM(val context: Context) : ViewModel() {

    fun click(value:String)
    {
        when(value)
        {
            "back" ->
            {
                (context as SetupProfile).onBackPressed()
            }
            "next" ->
            {
                context.startActivity(Intent(context, MainActivity::class.java) )
            }
        }
    }
}