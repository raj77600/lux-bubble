package com.luxbubbleuser.view.congScreen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.luxbubbleuser.R
import com.luxbubbleuser.databinding.ActivityCongoScreenBinding
import com.luxbubbleuser.databinding.ActivitySetupProfileBinding
import com.luxbubbleuser.view.setUpProfile.SetupProfileFactory
import com.luxbubbleuser.view.setUpProfile.SetupProfileVM

class CongoScreen : AppCompatActivity() {

    private var loginBinding: ActivityCongoScreenBinding? = null
    private lateinit var viewModel: CongoScreenVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        loginBinding = DataBindingUtil.setContentView(this, R.layout.activity_congo_screen)
        val factory = CongoFactory(this)
        viewModel = ViewModelProvider(this, factory).get(CongoScreenVM::class.java)
        loginBinding?.viewModel = viewModel
    }
}