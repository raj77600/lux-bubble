package com.luxbubbleuser.view.congScreen

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class CongoFactory(val context: Context) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        if (modelClass.isAssignableFrom(CongoScreenVM::class.java))
        {
            return CongoScreenVM(context) as T
        }
        throw IllegalArgumentException("")
    }
}