package com.luxbubbleuser.view.otpScreen

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class OtpScreenFactory(val context: Context) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(OtpScreenVM::class.java)) {
            return OtpScreenVM(context) as T
        }
        throw IllegalArgumentException("")
    }
}