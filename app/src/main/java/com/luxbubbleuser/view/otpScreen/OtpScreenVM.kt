package com.luxbubbleuser.view.otpScreen

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.luxbubbleuser.view.mainActivity.MainActivity

class OtpScreenVM(val context: Context) : ViewModel() {

    fun click(value:String)
    {
        when(value)
        {
            "back" ->
            {
                (context as OtpScreen).onBackPressed()
            }
            "verify" ->
            {
                context.startActivity(Intent(context, MainActivity::class.java) )
            }
        }
    }
}