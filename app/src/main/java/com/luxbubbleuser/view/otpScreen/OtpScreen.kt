package com.luxbubbleuser.view.otpScreen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.luxbubbleuser.R
import com.luxbubbleuser.databinding.ActivityLoginFirstBinding
import com.luxbubbleuser.databinding.ActivityOtpScreenBinding
import com.luxbubbleuser.view.login.loginNumber.LoginFirstFactory
import com.luxbubbleuser.view.login.loginNumber.LoginFirstVM

class OtpScreen : AppCompatActivity() {

    private var loginBinding: ActivityOtpScreenBinding? = null
    private lateinit var viewModel: OtpScreenVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_choose_login_signup)

        loginBinding = DataBindingUtil.setContentView(this, R.layout.activity_otp_screen)
        val factory = OtpScreenFactory(this)
        viewModel = ViewModelProvider(this, factory).get(OtpScreenVM::class.java)
        loginBinding?.viewModel = viewModel
    }
}