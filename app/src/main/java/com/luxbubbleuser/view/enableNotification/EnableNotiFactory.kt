package com.luxbubbleuser.view.enableNotification

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class EnableNotiFactory(val context: Context) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        if (modelClass.isAssignableFrom(EnableNotiVM::class.java))
        {
            return EnableNotiVM(context) as T
        }
        throw IllegalArgumentException("")
    }
}