package com.luxbubbleuser.view.enableNotification

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.luxbubbleuser.view.mainActivity.MainActivity

class EnableNotiVM(val context: Context) : ViewModel() {

    fun clicks(value:String)
    {
        when(value)
        {
            "back" ->
            {
                (context as EnableNotification).onBackPressed()
            }
            "noti" ->
            {
              (context as EnableNotification).startActivity(Intent(context, MainActivity::class.java))
                (context).finishAffinity()
            }
        }
    }

}