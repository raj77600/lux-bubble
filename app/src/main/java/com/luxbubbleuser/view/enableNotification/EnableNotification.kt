package com.luxbubbleuser.view.enableNotification

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.luxbubbleuser.R
import com.luxbubbleuser.databinding.ActivityEnableNotificationBinding

class EnableNotification : AppCompatActivity() {
    private var forgotBinding: ActivityEnableNotificationBinding? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        forgotBinding = DataBindingUtil.setContentView(this, R.layout.activity_enable_notification)
        val factory = EnableNotiFactory(this)
        val viewModel = ViewModelProvider(this, factory).get(EnableNotiVM::class.java)
        forgotBinding?.viewModel = viewModel
    }
}