package com.luxbubbleuser.networkcalls

import android.content.Context
import android.util.Log
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.luxbubbleuser.networkcalls.Urls.Companion.BASE_URL
import com.luxbubbleuser.utils.*
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import org.json.JSONObject
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object RetrofitCall {
    private var okHttpClient: OkHttpClient? = null
    private var retrofit: Retrofit? = null
    private var client: OkHttpClient.Builder? = null
    private var retrofitApi: RetrofitApi? = null
    private var coRoutineExceptionHandler: CoroutineExceptionHandler? = null

    fun <T> callService(
        context: Context,
        loader: Boolean,
        token: String,
        requestProcessor: RequestProcess<T>
    ) {
        try {

            if (!context.isNetworkAvailable()) {

                context.showNegativeAlerter(
                        "Your device seems to be offline!\nPlease connect to a working Internet Connection."
                )
                return
            }

            if (loader) {
                context.showDialog()
            }

            if (token == "") {
                okHttpClient = OkHttpClient.Builder()
                        .readTimeout(5, TimeUnit.MINUTES)
                        .connectTimeout(5, TimeUnit.MINUTES)
                        .build()
            } else {
                client = OkHttpClient.Builder()
                    .readTimeout(5, TimeUnit.MINUTES)
                    .connectTimeout(5, TimeUnit.MINUTES)
                    .addInterceptor { chain ->
                        val original = chain.request()

                        val request = original.newBuilder()
                            /*.header("Authorization", "Bearer $token")*/
                            .header("Authorization", token)
                            .method(original.method, original.body)
                            .build()

                        chain.proceed(request)
                    }

                okHttpClient = client?.build()

            }

            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient!!)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
            retrofitApi = retrofit?.create(RetrofitApi::class.java)

            coRoutineExceptionHandler = CoroutineExceptionHandler { _, t ->
                t.printStackTrace()

                CoroutineScope(Dispatchers.Main).launch {
                    context.hideDialog()
                    t.message?.let { requestProcessor.onError(it) }

                    if (t.message.equals("Unable to resolve host"))
                        context.showNegativeAlerter("Unable to resolve host")
                    else context.showNegativeAlerter("Server Error")
                }
            }

            CoroutineScope(Dispatchers.IO + coRoutineExceptionHandler!!).launch {

                val response = requestProcessor.sendRequest(retrofitApi!!) as Response<*>

                CoroutineScope(Dispatchers.Main).launch {
                    Log.e("resCodeIs", "====${retrofitApi.toString()}")
                    context.hideDialog()
                    if (!response.isSuccessful) {
                        val res = response.errorBody()!!.string()
                        Log.e("onError", "====$res")
                        requestProcessor.onError(res)
                        if (res == "Unauthorized") {
                            context.alert("Error", "Unauthorized")
                        } else {

                            val jsonObject = JSONObject(res)

                            Log.e("TAG", "response..... : $jsonObject")

                            when {
                                jsonObject.has("message") -> {
                                    val message = jsonObject.getString("message")
                                    context.showNegativeAlerter(message)

                                }
                                else -> {
                                    context.showNegativeAlerter("Server Error")
                                }
                            }
                        }

                    } else {
                        val code = response.body().toString()
                        Log.e("onSuccess", "====$code")
                        if (code.contains("Invalid Token")) {
                            context.alertToken("Error", "Session expired!! ", context)
                        } else {
                            requestProcessor.onResponse(response as T)
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: Throwable) {
            Log.e("TAG", "Server Error  $e")
        }
    }

}