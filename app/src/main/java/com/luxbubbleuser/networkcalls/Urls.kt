package com.luxbubbleuser.networkcalls

class Urls {

    companion object {
        const val BASE_URL = "http://15.207.74.128:3007/"

        const val SIGNUP = "api/app/signup"
        const val SIGNIN = "api/app/signin"
        const val SOCIAL_LOGIN = "api/app/socialLogin"
        const val FORGOT_PASSWORD = "api/app/forgotPassword"
        const val VERIFY_PHONE = "api/app/verifyPhone"
        const val RESEND_VERIFICATION = "api/app/resendVerification"
        const val UPDATE_PROFILE = "api/app/updateProfile"
        const val CHECK_VERIFICATION = "api/app/checkVerification"
        const val CHANGE_PASSWORD = "api/app/changePassword"
        const val RESET_PASSWORD = "api/app/resetPasswordPhone"
        const val LOGOUT = "api/app/logout"
    }

}