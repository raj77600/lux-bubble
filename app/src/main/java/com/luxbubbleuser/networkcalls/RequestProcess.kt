package com.luxbubbleuser.networkcalls

interface RequestProcess<T> {

    suspend fun sendRequest(retrofitApi: RetrofitApi): T

    fun onResponse(response: T)

    fun onError(message: String)

}