package com.luxbubbleuser.networkcalls

import com.google.gson.JsonElement
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface RetrofitApi {

    @FormUrlEncoded
    @POST(Urls.SIGNIN)
    suspend fun signInEmail(@Field("email") email: String, @Field("password") deviceType: String): Response<JsonElement>

    @FormUrlEncoded
    @POST(Urls.SIGNIN)
    suspend fun signInNumber(
            @Field("countryCode") countryCode: String,
            @Field("email") email: String,
            @Field("password") password: String): Response<JsonElement>

    @FormUrlEncoded
    @POST(Urls.SOCIAL_LOGIN)
    suspend fun socialLogin(
            @Field("email") email: String,
            @Field("firstName") firstName: String,
            @Field("lastName") lastName: String,
            @Field("provider") provider: String,
            @Field("providerId") providerId: String,
            @Field("phone") phone: String,
            @Field("deviceType") deviceType: String,
            @Field("pushToken") pushToken: String): Response<JsonElement>

    @FormUrlEncoded
    @POST(Urls.FORGOT_PASSWORD)
    suspend fun forgotEmail(@Field("email") email: String): Response<JsonElement>

    @FormUrlEncoded
    @POST(Urls.FORGOT_PASSWORD)
    suspend fun forgotNumber(
            @Field("countryCode") countryCode: String,
            @Field("email") email: String): Response<JsonElement>

    @FormUrlEncoded
    @POST(Urls.SIGNUP)
    suspend fun signUp(
            @Field("firstName") firstName: String,
            @Field("lastName") lastName: String,
            @Field("email") email: String,
            @Field("countryCode") countryCode: String,
            @Field("phone") phone: String,
            @Field("address") address: String,
            @Field("password") password: String,
            @Field("confirmPassword") confirmPassword: String,
            @Field("verificationBy") verificationBy: String,
            @Field("profilePic") profilePic: String,
            @Field("roles") roles: String
    ): Response<JsonElement>

    @Multipart
    @POST(Urls.SIGNUP)
    suspend fun signUpWithImage(
            @PartMap body: HashMap<String, RequestBody>,
            @Part part: MultipartBody.Part?
    ): Response<JsonElement>

    @FormUrlEncoded
    @PUT(Urls.UPDATE_PROFILE)
    suspend fun editProfile(
            @Field("firstName") firstName: String,
            @Field("lastName") lastName: String,
            @Field("email") email: String,
            @Field("countryCode") countryCode: String,
            @Field("phone") phone: String,
            @Field("address") address: String,
            @Field("roles") roles: String
    ): Response<JsonElement>

    @Multipart
    @PUT(Urls.UPDATE_PROFILE)
    suspend fun editWithImage(
            @PartMap body: HashMap<String, RequestBody>,
            @Part part: MultipartBody.Part?
    ): Response<JsonElement>

    @FormUrlEncoded
    @POST(Urls.RESEND_VERIFICATION)
    suspend fun resendEmailVerification(@Field("email") email: String): Response<JsonElement>

    @FormUrlEncoded
    @POST(Urls.CHECK_VERIFICATION)
    suspend fun checkEmailVerification(
            @Field("email") email: String,
            @Field("deviceType") deviceType: String,
            @Field("pushToken") pushToken: String): Response<JsonElement>

    @FormUrlEncoded
    @POST(Urls.RESEND_VERIFICATION)
    suspend fun resendNumberVerification(@Field("type") type: String,@Field("countryCode") countryCode: String, @Field("phone") phone: String): Response<JsonElement>

    @FormUrlEncoded
    @POST(Urls.VERIFY_PHONE)
    suspend fun verifyNumber(
            @Field("countryCode") countryCode: String,
            @Field("phone") phone: String,
            @Field("otp") otp: String,
            @Field("type") type: String,
            @Field("pushToken") pushToken: String,
            @Field("deviceType") deviceType: String
    ): Response<JsonElement>

    @FormUrlEncoded
    @POST(Urls.RESET_PASSWORD)
    suspend fun resetPassword(
            @Field("countryCode") countryCode: String,
            @Field("phone") phone: String,
            @Field("password") newPassword: String,
    ): Response<JsonElement>

    @FormUrlEncoded
    @PUT(Urls.CHANGE_PASSWORD)
    suspend fun changePassword(
            @Field("oldPassword") oldPassword: String,
            @Field("newPassword") newPassword: String,
            @Field("confirmPassword") confirmPassword: String,
    ): Response<JsonElement>

//    @PUT(Urls.updateProfile)
//    suspend fun updateProfile(@Body requestBody : RequestBody): Response<JsonElement>
}