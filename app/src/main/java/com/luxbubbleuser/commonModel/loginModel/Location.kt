package com.luxbubbleuser.commonModel.loginModel

data class Location(
        var coordinates: List<Int>,
        var type: String
)