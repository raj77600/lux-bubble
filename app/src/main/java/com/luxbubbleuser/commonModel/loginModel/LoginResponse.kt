package com.luxbubbleuser.commonModel.loginModel

import com.luxbubbleuser.commonModel.loginModel.Data

data class LoginResponse(
    var `data`: Data,
    var message: String,
    var success: Boolean
)