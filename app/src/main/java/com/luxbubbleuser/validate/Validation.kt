package com.mansour.com.validate

import android.app.Activity
import com.luxbubbleuser.utils.showNegativeAlerter

class Validation(private val baseActivity: Activity?) {
    private var validators: ArrayList<Validator>? = null

    init {
        validators = ArrayList()
    }

    companion object {
        fun build(baseActivity: Activity?): Validation {
            return Validation(baseActivity)
        }
    }

    fun isEmpty(editable: String?, message: String): Validation? {
        validators?.add(EmptyValidator(editable ?: "", message))
        return this
    }

    fun isEmailValid(editable: String?, message: String): Validation? {
        validators?.add(EmailValidator(editable ?: "", message))
        return this
    }

    fun isPhoneValid(editable: String?, message: String): Validation? {
        validators?.add(PhoneValidator(editable ?: "", message))
        return this
    }

    fun isValidPassword(editable: String?, message: String): Validation? {
        validators?.add(PasswordValidator(editable ?: "", message))
        return this
    }

    fun areEqual(editable: String?, editable2: String?, message: String): Validation? {
        validators?.add(CompareValidator(editable ?: "", editable2 ?: "", message))
        return this
    }

    fun isValid(): Boolean {
        validators.let {
            it?.forEach { validator ->
                if (!validator.isValid()) {
                    baseActivity?.showNegativeAlerter(validator.message() ?: "")
                    return false
                }
            }
        }
        return true
    }

}