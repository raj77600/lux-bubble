package com.mansour.com.validate

interface Validator {
    fun isValid(): Boolean
    fun message(): String?
}